module.exports = {
  branches: ["main"],
  repositoryUrl: "git@gitlab.com:flipactual/ilo.git",
  plugins: [
    ["@semantic-release/commit-analyzer"],
    "@semantic-release/release-notes-generator",
    "@semantic-release/changelog",
    [
      "@semantic-release/npm",
      {
        pkgRoot: "./pkg"
      }
    ],
    [
      "@semantic-release/gitlab",
      {
        assets: ["CHANGELOG.md"]
      }
    ]
  ]
};
