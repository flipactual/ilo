# ilo

ilo is an alternative functional interface for built-in JavaScript methods

```js
// native
[1, 2, 3].map((x) => x * 2);

// ilo
map((x) => x * 2)([1, 2, 3]);
// where `map` is `(fn) => (xs) => xs.map(fn)`
```

