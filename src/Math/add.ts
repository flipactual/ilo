/**
 * Adds two numbers
 *
 * `add :: number -> number -> number`
 */
export function add(x: number): (y: number) => number {
  return (y: number): number => x + y;
}
